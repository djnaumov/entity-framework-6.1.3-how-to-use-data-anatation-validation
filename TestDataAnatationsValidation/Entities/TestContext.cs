﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestDataAnatationsValidation.Entities
{
    public class TestContext : DbContext
    {
        public TestContext():base("name=TestContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<TestContext>());
        }
        public DbSet<Product> Products { get; set; }
    }
}