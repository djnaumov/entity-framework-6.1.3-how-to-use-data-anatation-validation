﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestDataAnatationsValidation.Entities
{
    public class Product
    {
        public Product()
        {
            Id = -1;
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

       public bool IsNew { get { return Id == -1; } } 

        [Required]
        [StringLength(3)]
        public string Name { get; set; }

        [Required]
        public double Price { get; set; }
    }
}