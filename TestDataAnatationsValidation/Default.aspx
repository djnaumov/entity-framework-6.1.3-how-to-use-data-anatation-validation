﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TestDataAnatationsValidation.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TestContext</title>
</head>
<body>
    <form id="form1" runat="server">
        <h3>Create product</h3>
        <div>
            <asp:Label runat="server" ID="Lable1" Text="Name: "/>
            <asp:TextBox runat="server" ID="TextBoxName" />
        </div>
        <div>
            <asp:Label runat="server" ID="Label2" Text="Price: " />
            <asp:TextBox runat="server" ID="TextBoxPrice" />
        </div>
        <div>
            <asp:Label runat="server" ID="MessageLabel"></asp:Label>
        </div>
    </form>
</body>
</html>
