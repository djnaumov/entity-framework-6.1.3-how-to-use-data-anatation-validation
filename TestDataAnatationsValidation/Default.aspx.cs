﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TestDataAnatationsValidation.Entities;

namespace TestDataAnatationsValidation
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Button submitButton = new Button();
            submitButton.Text = "Create";
            submitButton.Click += submitButton_Click;
            form1.Controls.Add(submitButton);
        }

        void submitButton_Click(object sender, EventArgs e)
        {
            try
            {
                Product product = new Product();
                product.Name = TextBoxName.Text;
                product.Price = string.IsNullOrEmpty(TextBoxPrice.Text) ? 0 : Convert.ToDouble(TextBoxPrice.Text);


                using (TestContext context = new TestContext())
                {
                    DbEntityValidationResult result = context.Entry(product).GetValidationResult();
                    if (result.IsValid)
                    {
                        context.Products.Add(product);
                        context.SaveChanges();
                    }
                    else
                    {
                        string[] errors = result.ValidationErrors.Select(x => x.ErrorMessage).ToArray();
                        MessageLabel.Text = String.Join(Environment.NewLine, errors);
                    }
                }
            }
            catch (Exception exception)
            {
                MessageLabel.Text = exception.Message;
            }


        }
    }
}